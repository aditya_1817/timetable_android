package com.example.timetablegenerator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

public class Main3Activity extends AppCompatActivity {

    Button Viewtimetable;
    Button SeeNotification;

    String[] urls = new String[2];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        Viewtimetable = (Button) findViewById(R.id.button4);
        SeeNotification = (Button) findViewById(R.id.button5);

        urls[0] = "http://timetablecreator.000webhostapp.com/staff_section/staff_login.php";
        urls[1] = "http://timetablecreator.000webhostapp.com/staff_section/staff_login.php";

        Viewtimetable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), WebActivity.class);
                intent.putExtra("links", urls[0]);
                startActivity(intent);
            }
        });

        SeeNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), WebActivity.class);
                intent.putExtra("links", urls[1]);
                startActivity(intent);
            }
        });

    }


}



